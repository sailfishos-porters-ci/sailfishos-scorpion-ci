# Install SailfishOS on Xperia Z3 Tablet Compact (SGP621)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)


## Latest build
[![pipeline status](https://gitlab.com/sailfishos-porters-ci/sailfishos-scorpion-ci/badges/master/pipeline.svg)](http://gitlab.com/sailfishos-porters-ci/sailfishos-scorpion-ci/-/jobs/artifacts/master/browse?job=run-build)


## Install
**!!!! Backup your data you do it on your own risk !!!!**

* unlock the bootloader check [Sony Open Devices](https://developer.sony.com/develop/open-devices/)
* flash [AOSP 6.0.1_r80](https://nokius.net/SFOS/scorpion/AOSP/scorpion_android-6.0.1_r80_modem-fix.tar) on your device.

```
fastboot -S 256M flash boot boot.img
fastboot -S 256M flash system system.img
fastboot -S 256M flash userdata userdata.img
```
* flash [twrp](https://androidfilehost.com/?fid=4996503000517182028) 
```
fastboot -S 256M flash recovery recovery-twrp-3.0.2-0-scorpion.img
```

* reboot your device
```
fastboot reboot
```

* enter recovery with press volume down while the LED is purple

* sideload Sailfish OS

```
adb sideload sailfishos-scorpion-release-2.2.1.18-Alpha1.zip
```

* reboot and enjoy 

thanks to r0kk3rz for the great work!